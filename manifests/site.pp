Package {
	allow_virtual => false
}

file { '/tmp/jdk-7u67-linux-x64.rpm':
	source => '/vagrant/files/jdk-7u67-linux-x64.rpm',
}

package { 'jdk-1.7.0_67-fcs.x86_64':
	provider => rpm,
	ensure => present,
	source => '/tmp/jdk-7u67-linux-x64.rpm',
	require => File['/tmp/jdk-7u67-linux-x64.rpm'],
}

file { '/tmp/wso2am-1.7.0.zip':
	source => '/vagrant/files/wso2am-1.7.0.zip',
}

file { '/opt/wso2am-1.7.0':
	ensure => directory,
	owner  => 'vagrant',
	group  => 'vagrant',
	mode   => 0644,
}

exec { 'Extract WSO2 API Manager':
	command => '/usr/bin/unzip /tmp/wso2am-1.7.0.zip',
	cwd     => '/opt',
	creates => '/opt/wso2am-1.7.0/bin/wso2server.sh',
	user    => 'vagrant',
	group   => 'vagrant',
	require => File['/tmp/wso2am-1.7.0.zip', '/opt/wso2am-1.7.0'],
	timeout => 0,
}->
file {'/etc/init.d/wso2am':
	owner => root,
	group => root,
	mode => 755,
	source => '/vagrant/files/wso2am',	
}->
service { 'wso2am':
	ensure => true,
	enable => true,
        require => Package['jdk-1.7.0_67-fcs.x86_64'],
}

file { '/opt/wso2am-1.7.0/repository/conf/api-manager.xml':
	source => '/vagrant/files/api-manager.xml',
	require => Exec['Extract WSO2 API Manager'],
	notify => Service['wso2am'],
}

file { '/opt/wso2am-1.7.0/repository/conf/datasources/master-datasources.xml':
	source => '/vagrant/files/master-datasources.xml',
	require => Exec['Extract WSO2 API Manager'],
	notify => Service['wso2am'],
}

file { '/tmp/wso2bam-2.4.1.zip':
	source => '/vagrant/files/wso2bam-2.4.1.zip',
}

file { '/opt/wso2bam-2.4.1':
	ensure => directory,
	owner  => 'vagrant',
	group  => 'vagrant',
	mode   => 0644,
}

exec { 'Extract WSO2 Business Manager':
	command => '/usr/bin/unzip /tmp/wso2bam-2.4.1.zip',
	cwd     => '/opt',
	creates => '/opt/wso2bam-2.4.1/bin/wso2server.sh',
	user    => 'vagrant',
	group   => 'vagrant',
	require => File['/tmp/wso2bam-2.4.1.zip', '/opt/wso2bam-2.4.1'],
	timeout => 0,
}->
file {'/etc/init.d/wso2bam':
	owner => root,
	group => root,
	mode => 755,
	source => '/vagrant/files/wso2bam',	
}->
service { 'wso2bam':
	ensure => true,
	enable => true,
        require => [ Package['jdk-1.7.0_67-fcs.x86_64'], Service['wso2am'] ],
}

file { '/opt/wso2bam-2.4.1/repository/conf/carbon.xml':
	source => '/vagrant/files/carbon.xml',
	require => Exec['Extract WSO2 Business Manager'],
	notify => Service['wso2bam'],
}

file { '/opt/wso2bam-2.4.1/repository/conf/datasources/bam-datasources.xml':
	source => '/vagrant/files/bam-datasources.xml',
	require => Exec['Extract WSO2 Business Manager'],
	notify => Service['wso2bam'],
}

file { '/opt/wso2bam-2.4.1/repository/deployment/server/bam-toolbox/API_Manager_Analytics.tbox':
	source => '/vagrant/files/API_Manager_Analytics.tbox',
	require => Exec['Extract WSO2 Business Manager', 'Extract WSO2 API Manager'],
	notify => Service['wso2bam'],
}

file { '/opt/wso2bam-2.4.1/repository/conf/etc/hector-config.xml':
	source => '/vagrant/files/hector-config.xml',
	require => Exec['Extract WSO2 Business Manager'],
	notify => Service['wso2bam'],
}

file { '/home/vagrant/.bashrc':
	source => '/vagrant/files/bashrc',
}

service { 'iptables':
	ensure => false,
	enable => false,
}
